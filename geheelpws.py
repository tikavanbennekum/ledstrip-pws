from gtts import gTTS
import speech_recognition as sr
from pygame import mixer
import random
import time
import RPi.GPIO as GPIO

ledpins = [16, 19, 20, 21]
GPIO.setmode(GPIO.BCM)

for pin in ledpins:
	GPIO.setup(pin, GPIO.OUT)

def uit():
  for pin in ledpins:
  	GPIO.output(pin, GPIO.LOW)

#def talk(audio):
#    """spreekt woorden uit"""
#    print(audio)
#    for line in audio.splitlines():
#        text_to_speech = gTTS(text=audio, lang='en-uk')
#        text_to_speech.save('audio.mp3')
#        mixer.init()
#        mixer.music.load("audio.mp3")
#        mixer.music.play()

def myCommand():
    """geeft de gesproken tekst terug als string"""
    #Initialize the recognizer
    #The primary purpose of a Recognizer instance is, of course, to recognize speech.
    r = sr.Recognizer()

    with sr.Microphone() as source:
        print('TARS is Ready...')
        r.pause_threshold = 2
        #wait for a second to let the recognizer adjust the
        #energy threshold based on the surrounding noise level
        r.adjust_for_ambient_noise(source, duration=1)
        #listens for the user's input
        audio = r.listen(source)

    try:
        command = r.recognize_google(audio).lower()
        print('You said: ' + command + '\n')

    #loop back to continue to listen for commands if unrecognizable speech is received
    except sr.UnknownValueError:
        print('Your last command couldn\'t be heard')
        command = myCommand();
    return command

def tars(command):
    """verwerkt uitgesproken tekst"""
    errors=[
        "I don't know what you mean",
        "Did you mean astronaut?",
        "Can you repeat it please?",
    ]
    if 'green' in command:
        GPIO.output(16, GPIO.HIGH)
        print('Hello! I am TARS. How can I help you?')
    elif 'purple' in command:
        GPIO.output(19, GPIO.HIGH)
        print('Hello! I am TARS. How can I help you?')
    elif 'blue' in command:
        GPIO.output(20, GPIO.HIGH)
        print('Hello! I am TARS. How can I help you?')
    elif 'yellow' in command:
        GPIO.output(21, GPIO.HIGH)
        print('Hello! I am TARS. How can I help you?')
    else:
        error = random.choice(errors)
        print(error)


print('TARS is ready!')

#loop to continue executing multiple commands
while True:
    tars(myCommand())

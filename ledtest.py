import RPi.GPIO as GPIO
import time

ledpins = [16, 19, 20, 21]

# setup
GPIO.setmode(GPIO.BCM)

for pin in ledpins:
	GPIO.setup(pin, GPIO.OUT)

def uit():
	for pin in ledpins:
		GPIO.output(pin, GPIO.LOW)

# animatie
while True:
	for pin in ledpins:
		uit()
		GPIO.output(pin, GPIO.HIGH)
		time.sleep(2)

GPIO.cleanup()
